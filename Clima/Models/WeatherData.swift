import Foundation

struct WeatherData: Codable {
    let name: String
    let main: MainDetails
    let weather: [Weather]
}

struct MainDetails: Codable {
    let temp: Double
}

struct Weather: Codable {
    let id: Int
    let main: String
    let description: String
}
